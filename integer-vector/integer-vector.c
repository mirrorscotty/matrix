#include <integer-vector.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

intVector* CreateIntVector(int length)
{
    assert(length > 0);

    intVector *a;
    a = calloc(sizeof(intVector), 1);
    a->length = length;
    a->data = calloc(sizeof(int), length);

    return a;
}

void DestroyIntVector(intVector *a)
{
    assert(a);

    free(a->data);
    free(a);
}

void SetValIntVector(intVector *a, int position, int value)
{
    assert(a);
    assert(position >= 0);
    assert(position < a->length);

    a->data[position] = value;
}

int GetValIntVector(intVector *a, int position)
{
    assert(a);
    assert(position >= 0);
    assert(position < a->length);

    return a->data[position];
}

void PrintIntVector(intVector *a)
{
    assert(a);

    int i;
    puts("[");
    for(i=0; i < a->length; i++)
        printf("%d\n", GetValIntVector(a, i));
    puts("]");
}

