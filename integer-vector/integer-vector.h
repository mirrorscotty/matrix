/**
 * @file string-vector.h
 * Define a struct to contain a vector of integers
 */

#ifndef INTEGER_VECTOR_H 
#define INTEGER_VECTOR_H 

/**
 * @struct intVector
 * @brief A data structure to store an array of integers.
 * @var intVector::v
 * A pointer to the raw data
 * @var intVector::length
 * Number of components in the vector
 */
typedef struct {
    int *data;
    int length;
} intVector;

intVector* CreateIntVector(int);
void DestroyIntVector(intVector*);
void SetValIntVector(intVector*, int, int);
int GetValIntVector(intVector*, int);
void PrintIntVector(intVector*);

#endif

