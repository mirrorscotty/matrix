#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "bandmatrix.h"
#include "../matrix.h"

bndmatrix* CreateBandMatrix(int rows, int bandwidth)
{
    int i;
    bndmatrix *bm;

    assert(rows > 0);
    assert(bandwidth >= 0);

    bm = (bndmatrix*) calloc(1, sizeof(bndmatrix));

    bm->r = rows;
    bm->bw = bandwidth;
    bm->maxbw = 2*rows-1;

    bm->data = (double**) calloc(bm->maxbw, sizeof(double*));
    /* If the supplied bandwidth is zero, don't allocate any additional memory.
     * We'll do that later on as needed. */
    if(bandwidth > 0)
        for(i=(bm->maxbw-bandwidth)/2; i<(bm->maxbw+bandwidth)/2; i++)
            bm->data[i] = (double*) calloc(rows, sizeof(double));

    return bm;
}

void DestroyBandMatrix(bndmatrix *bm)
{
    int i;

    assert(bm);
    
    for(i=0; i<bm->maxbw; i++)
        if(bm->data[i]) /* Only free a diagonal if it was allocated. */
            free(bm->data[i]);
    free(bm);
}

double valB(bndmatrix *bm, int row, int col)
{
    int offset, diag;

    assert(bm);
    assert(row >= 0);
    assert(col >= 0);
    assert(row < bm->r);
    assert(col < bm->r);

    diag=bm->r;
    offset=col-row;

    if(bm->data[diag+offset])
        return bm->data[diag+offset][row];
    else
        return 0;

}

double setvalB(bndmatrix *bm, double val, int row, int col)
{
    int diag, offset;

    assert(bm);
    assert(row >= 0);
    assert(col >= 0);
    assert(row < bm->r);
    assert(col < bm->r);

    diag=bm->r-1;
    offset=col-row;

    if(bm->data[diag+offset] == NULL) {
        bm->data[diag+offset] = (double*) calloc(bm->r, sizeof(double));
        if(offset*2 > bm->bw)
            bm->bw = offset*2;
    }

    bm->data[diag+offset][row] = val;

    return val;
}

bndmatrix* MatrixToBand(matrix *src)
{
    int i, j;
    double value;
    bndmatrix *dest;

    assert(src);

    dest = CreateBandMatrix(nRows(src), 0);

    for(i=0; i<nRows(src); i++) {
        for(j=0; j<nCols(src); j++) {
            value = val(src, i, j);
            if(value != 0)
                setvalB(dest, value, i, j);
        }
    }

    return dest;
}

void bandprnt(bndmatrix *bm)
{
    int diag, row;
    double v;

    assert(bm);

    for(row=0; row<bm->r; row++) {
        printf("[ ");
        for(diag=0; diag<bm->maxbw; diag++) {
            if(bm->data[diag] == NULL) {
                printf("- ");
            } else {
                v = bm->data[diag][row];
                if(fabs(v) < 1e-14)
                    printf("%e ", 0.0);
                else
                    printf("%e ", v);
            }
        }
        printf("]\n");
    }
}

