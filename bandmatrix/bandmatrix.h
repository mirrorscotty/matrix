#ifndef BANDMATRIX_H
#define BANDMATRIX_H

#include "../matrix.h"

typedef struct {
    double **data; /* Raw data */
    int r; /* Rows */
    int bw; /* Bandwidth */
    int maxbw; /* Maximum bandwidth */
} bndmatrix;

bndmatrix* CreateBandMatrix(int, int);
void DestroyBandMatrix(bndmatrix*);
double valB(bndmatrix*, int, int);
double setvalB(bndmatrix*, double, int, int);
bndmatrix* MatrixToBand(matrix*);
void bandprnt(bndmatrix*);

#endif

