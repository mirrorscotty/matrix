VPATH=2dmatrix vector bandmatrix string-vector integer-vector
CC=gcc
CFLAGS=-I2dmatrix -Ibandmatrix -Istring-vector -Iinteger-vector
OBJ=2dmatrix/2dmatrix.o 2dmatrix/2dmatrixio.o 2dmatrix/2dmatrixops.o 2dmatrix/mtxsolver.o 2dmatrix/xstrtok.o vector/vector.o vector/vectorio.o vector/vectorops.o other.o bandmatrix/bandmatrix.o string-vector/string-vector.o integer-vector/integer-vector.o

all: CFLAGS += -DNDEBUG -O2
all: matrix.a

debug: CFLAGS += -ggdb -O0 -Wstrict-prototypes -Werror -Wall
debug: matrix.a

matrix.a: $(OBJ)
	ar -cvr $@ $?

doc: Doxyfile
	doxygen Doxyfile

clean:
	rm -rf matrix.a doc
	rm -rf $(OBJ)
	rm -rf $(OBJ:.o=.d)

%.o: %.c
	$(CC) -c $(CFLAGS) $*.c -o $*.o
	$(CC) -MM $(CFLAGS) $*.c > $*.d
	@mv -f $*.d $*.d.tmp
	@sed -e 's|.*:|$*.o:|' < $*.d.tmp > $*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $*.d.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $*.d
	@rm -f $*.d.tmp

-include $(OBJ:.o=.d)
