#include <string-vector.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

strVector* CreateStrVector(int length)
{
    assert(length > 0);

    strVector *a;
    a = calloc(sizeof(strVector), 1);
    a->length = length;
    a->data = calloc(sizeof(char*), length);

    return a;
}

void DestroyStrVector(strVector *a)
{
    assert(a);

    int i;
    for(i=0; i < a->length; i++) {
        if(a->data[i])
            free(a->data[i]);
    }
    free(a->data);
    free(a);
}

void InsertValStrVector(strVector *a, int position, char *str)
{
    assert(a);
    assert(position >= 0);
    assert(position < a->length);
    assert(str);

    if(a->data[position]) /* If there's already something there, quit */
        return;
    a->data[position] = str;
}

void CopyValStrVector(strVector *a, int position, char *str, int len)
{
    assert(a);
    assert(position >= 0);
    assert(position < a->length);
    assert(str);
    assert(len > 0);

    if(len == 0)
       for(len=0; str[len]=='\0'; len++) { /* Find the length of the string */
       }
    if(a->data[position])
        free(a->data[position]);
    a->data[position] = calloc(sizeof(char*), len+1);
    strncpy(a->data[position], str, len);
}

void PrintStrVector(strVector *a)
{
    assert(a);

    int i;
    puts("[");
    for(i=0; i < a->length; i++)
        if(a->data[i])
            puts(a->data[i]);
        else
            puts("NULL");
    puts("]");
}

