/**
 * @file string-vector.h
 * Define a struct to contain a vector of fixed-length strings
 */

#ifndef STRING_VECTOR_H
#define STRING_VECTOR_H

/**
 * @struct strVector
 * @brief A data structure to store an array of strings.
 * @var strVector::v
 * A pointer to the raw data
 * @var strVector::length
 * Number of components in the vector
 */
typedef struct {
    char **data;
    int length;
} strVector;

strVector* CreateStrVector(int);
void DestroyStrVector(strVector*);
void CopyValStrVector(strVector*, int, char*, int);
void InsertValStrVector(strVector*, int, char*);
void PrintStrVector(strVector*);

#endif

